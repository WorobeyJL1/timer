/*
Author: Jonathan Worobey
Date: 3/20/2016

*/
#pragma once
#include <Windows.h>
#include <ostream>

class Timer {
public:
	Timer();
	void start();
	long long stop();
	long long pause();
	long long getTime();
	void resume();

	void printTime(std::ostream &out) const;
private:
	friend std::ostream& operator<<(std::ostream &os, const Timer& p);
	LARGE_INTEGER frequency;
	LARGE_INTEGER startTime;
	LARGE_INTEGER stopTime;
	LARGE_INTEGER pauseTime;
	LARGE_INTEGER elapsed;
	bool paused;
};