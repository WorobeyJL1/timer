Author: Jonathan Worobey
Date: 3/20/2016

Dependencies:
	<Windows.h>
	<ostream>

Create a Timer object:
	Timer t;
	
Start the timer:
	t.start(); // returns void

Do timed work here.

Pause the timer:
	t.pause(); //returns elapsed time in miliseconds (long long).
	
Time spent here is subtracted from the elapsed time.

Resume the timer:
	t.resume(); // returns void
	
More timed work.

Stop the timer:
	t.stop; //returns elapsed time in miliseconds (long long).
	
Output results:
	cout << t << endl; // prints with appropriate units (microseconds, ms, s, min, or h). If you do not want units applied, use the microseconds returned from stop() or pause().

Get time without pausing the timer (some overhead involved)
	t.getTime(); //returns elapsed time in miliseconds (long long).
	
Note: there is, of course, some overhead in pausing and resuming the timer, avoid these if you want the highest precision.