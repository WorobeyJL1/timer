/*
Author: Jonathan Worobey
Date: 3/20/2016

*/

#include "Timer.h"

Timer::Timer()
{
	QueryPerformanceFrequency(&frequency);
	startTime.QuadPart = 0;
	stopTime.QuadPart = 0;
	pauseTime.QuadPart = 0;
	elapsed.QuadPart = 0;
	paused = false;
}

void Timer::start()
{
	stopTime.QuadPart = 0;
	pauseTime.QuadPart = 0;
	elapsed.QuadPart = 0;
	paused = false;
	QueryPerformanceCounter(&startTime);
}

long long Timer::stop()//returns microseconds
{
	QueryPerformanceCounter(&stopTime);
	elapsed.QuadPart = stopTime.QuadPart - startTime.QuadPart;
	elapsed.QuadPart *= 1000000;
	elapsed.QuadPart /= frequency.QuadPart;

	return elapsed.QuadPart;
}

long long Timer::pause()//returns microseconds
{
	if(paused == false)
	{
		paused = true;
		QueryPerformanceCounter(&pauseTime);
		elapsed.QuadPart = pauseTime.QuadPart - startTime.QuadPart;
		elapsed.QuadPart *= 1000000;
		elapsed.QuadPart /= frequency.QuadPart;
	}
	return elapsed.QuadPart;
}

void Timer::resume()
{
	if(paused == true)
	{
		paused = false;
		LARGE_INTEGER temp;
		QueryPerformanceCounter(&temp);
		startTime.QuadPart = startTime.QuadPart + temp.QuadPart - pauseTime.QuadPart;
	}
	else
		return;
}

void Timer::printTime(std::ostream &out) const
{
	if(elapsed.QuadPart < 1000)///microseconds
	{
		out << elapsed.QuadPart << " microseconds";
	}
	else if(elapsed.QuadPart < 1000000)///miliseconds
	{
		out << elapsed.QuadPart / 1000 << "." << elapsed.QuadPart % 1000 << " ms";
	}
	else if(elapsed.QuadPart < 60000000)///seconds
	{
		out << elapsed.QuadPart / 1000000 << "." << elapsed.QuadPart % 1000000 << " s";
	}
	else if(elapsed.QuadPart < 3600000000)///minutes
	{
		out << elapsed.QuadPart / 60000000 << "." << elapsed.QuadPart % 60000000 << " min";
	}
	else /// hours
	{
		out << elapsed.QuadPart / 3600000000 << "." << elapsed.QuadPart % 3600000000 << " h";
	}
}

std::ostream& operator<<(std::ostream &out, const Timer& p)
{
	p.printTime(out);
	return out;
}

long long Timer::getTime()
{
	LARGE_INTEGER temp;
	LARGE_INTEGER temp2;
	QueryPerformanceCounter(&temp);
	temp2.QuadPart = temp.QuadPart - startTime.QuadPart;
	temp2.QuadPart *= 1000000;
	temp2.QuadPart /= frequency.QuadPart;
	return temp2.QuadPart;
}